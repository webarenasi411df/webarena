<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
App::uses('AppModel', 'Model');

class Fighter extends AppModel {

    public $displayField = 'name';
    public $belongsTo = array(
        'Player' => array(
            'className' => 'Player',
            'foreignKey' => 'player_id',
        ),
    );

     public function havePerso($id)
    {
        $persos = $this->find('first',
                array('conditions' => array('Fighter.player_id' => $id)));
        if (is_null($persos))
        {
            return null;
        } else
        {
            return $persos;
        }
    }
    public function checkVacancy($fighter_Id, $direction) {
        
    }

    public function checkLimits($fighter_Id, $direction) {
        
    }

    public function doMove($fighterId, $direction) {

        $datas = $this->read(null, $fighterId);

        // falre la modif
        if ($direction == 'north' && ($datas['Fighter']['coordinate_y'])>0 ) {
            $this->set('coordinate_y', $datas['Fighter']['coordinate_y'] - 1);
        } elseif ($direction == 'south' && ($datas['Fighter']['coordinate_y'])<9) {
            $this->set('coordinate_y', $datas['Fighter']['coordinate_y'] + 1);
        } elseif ($direction == 'east' && ($datas['Fighter']['coordinate_x'])<14) {
            $this->set('coordinate_x', $datas['Fighter']['coordinate_x'] + 1);
        } elseif ($direction == 'west' && ($datas['Fighter']['coordinate_x'])>0) {
            $this->set('coordinate_x', $datas['Fighter']['coordinate_x'] - 1);
        } else {
            return false;
        }

        // sauver la modif
        $this->save();
        return true;
    }

    
    public function successAttack($fighter_Id, $opponent_Id) {
        
       $fighter= $this->find('first', array('conditions' => array('Fighter.id' => $fighter_Id)));
       $opponent= $this->find('first', array('conditions' => array('Fighter.id' => $opponent_Id)));

       $level_fighter = $fighter['Fighter']['level'];
       $level_opponent = $opponent['Fighter']['level'];

        $success = 10 + $level_fighter - $level_opponent;
        $chance = rand(0, 20);

        if ($chance > $success) {
            
           return 1;
           
        } else {
            
            return 0;
        }
        
        
    }
    
    
    public function doAttack($fighter_Id, $direction) {
        
        $fighter = $this->find("first", array("conditions" => array("Fighter.id" => $fighter_Id)));
        //pr($fighter);


        // falre la modif
        switch ($direction) {
            case 'north':
                
                $opponent = $this->find("first", array('fields' => array('Fighter.id','Fighter.name','Fighter.skill_strength'),
                'conditions' => array("Fighter.coordinate_y" => $fighter["Fighter"]["coordinate_y"] - 1,
                    "Fighter.coordinate_x" => $fighter["Fighter"]["coordinate_x"])));
                //echo 'Vous avez un ennemi au nord: ';
                break;
            case 'south':
 
                $opponent = $this->find("first", array('fields' => array('Fighter.id','Fighter.name','Fighter.skill_strength'),
                'conditions' => array("Fighter.coordinate_y" => $fighter["Fighter"]["coordinate_y"] + 1,
                    "Fighter.coordinate_x" => $fighter["Fighter"]["coordinate_x"])));
                //echo 'Vous avez un ennemi au sud: ';
                break;
            case 'east':

                $opponent = $this->find("first", array('fields' => array('Fighter.id','Fighter.name','Fighter.skill_strength'),
               'conditions' => array("Fighter.coordinate_y" => $fighter["Fighter"]["coordinate_y"],
                    "Fighter.coordinate_x" => $fighter["Fighter"]["coordinate_x"] + 1)));
                //echo 'Vous avez un ennemi a l\'est: ';
                
                break;
            case 'west':
                
                $opponent = $this->find("first", array('fields' => array('Fighter.id','Fighter.name','Fighter.skill_strength'),
                'conditions' => array("Fighter.coordinate_y" => $fighter["Fighter"]["coordinate_y"],
                    "Fighter.coordinate_x" => $fighter["Fighter"]["coordinate_x"] - 1)));
                //echo 'Vous avez un ennemi a l\'ouest: ';
                break;
            default:
                return false;
        }
        
        if (!empty($opponent))
            {
                $opponent_Id = $opponent['Fighter']['id']; echo $opponent_Id."   ";
                $opponent_skillstrength = $opponent['Fighter']['skill_strength']; 
                
                $attaque = $this->successAttack($fighter_Id, $opponent_Id);
                
                $fighter_skillstrength = $fighter['Fighter']['skill_strength'];
                
                $this->changeXpFighter($fighter_Id, $attaque, $opponent_skillstrength);
                $this->changeXpOpponent($opponent_Id, $attaque, $fighter_skillstrength);
                
                //$this->deathFighter($fighter_Id);
                
                $result = array( 'name_opponent' => $opponent['Fighter']['name'], 'attack_outcome' => $attaque);
                
            }
            
        return $result;
    }


    public function changeXpFighter($fighter_Id, $attaque, $damage) {

        
        $fighter= $this->read(null, $fighter_Id);

        if ($attaque  == 1) {

            //echo " Victoire!" ;
            $this->set('xp', $fighter['Fighter']['xp'] + 1);
            //echo " Vous gagnez un point d'experience";
            
            
        }elseif ($attaque == 0) {
            
            //echo " Vous avez perdu";
            $this->set('current_health', $fighter['Fighter']['current_health'] - $damage );
            
        }
        
        $this->save();
    }
    
    public function changeXpOpponent($opponent_Id, $attaque, $damage) {

        
        $opponent = $this->read(null, $opponent_Id);

        if ($attaque  == 0) {

            $this->set('xp', $opponent['Fighter']['xp'] + 1);
            //echo "  Votre ennemi gagne un point d'experience";
            
            
        }elseif ($attaque == 1) {
            
            $this->set('current_health', $opponent['Fighter']['current_health'] - $damage );
            
        }
        
        $this->save();
    }
    
    public function levelUp($fighter_Id, $choice){
        
        $fighter= $this->read(null, $fighter_Id);
        $xp = $fighter['Fighter']['xp'];
        
        if ($xp%4 == 0) {

                $this->set('level', $fighter['Fighter']['level'] + 1);
                //echo " Vous avez passe un niveau";
                
                if( $choice == 'sight' ){
                    
                    $this->set('skill_sight', $fighter['Fighter']['skill_sight'] + 1);
                    
                    
                }elseif( $choice == 'strength' ){
                    
                    $this->set('skill_strength', $fighter['Fighter']['skill_strength'] + 1);
                    
                    
                }elseif( $choice == 'health' ){
                    
                    $this->set('current_health', $fighter['Fighter']['current_health'] + 3);
                    
                    
                }
                
            }
            
            $this->save();
            
       
    }
    
    public function deathFighter($fighter_Id){
        
        $fighter= $this->read(null, $fighter_Id);
        $fighter_currenthealth = $fighter['Fighter']['current_health'];
        
        if ( $fighter_currenthealth == 0 ){
           
           $mort =  1;
           $this->delete($fighter_Id);
           
        }else{
            
            $mort = 0;
        }
        
        return $mort;
    }
    
    public function deathFighterArena($fighter_Id){
        
        $this->delete($fighter_Id);
        
        
    }

    public function newFighter($nomFighter, $kind,$player_id) {


        $this->create();
        $this->save($this->data);

        $this->set('name', $nomFighter);
        $this->set('player_id', $player_id);
        $this->set('coordinate_x', 3);
        $this->set('coordinate_y', 3);
        $this->set('level', 3);
        $this->set('xp', 10);
        $this->set('skill_sight', 0);
        $this->set('skill_strength', 1);
        $this->set('skill_health', 3);
        $this->set('current_health', 3);
        $this->set('next_action_time', '0000-00-00 00:00:00');

        $this->save($this->data);


        //$path = '../webroot/img/avatar/' . $nomFighter . '.png';
        $path2 = '../webroot/img/avatar/' . $nomFighter . '.png';
       
        switch ($kind)
        {
            case 'sacrieurM':
                //copy('../webroot/img/avatar/tete_jeune_m.png', $path);
                copy('../webroot/img/avatar/fighter_jeune_m.png', $path2);
                break;
            case 'sacrieurF':
                //copy('../webroot/img/avatar/tete_jeune_f.png', $path);
                copy('../webroot/img/avatar/fighter_jeune_f.png', $path2);

                break;
            case 'steamerM':
                //copy('../webroot/img/avatar/tete_punk_m.png', $path);
                copy('../webroot/img/avatar/fighter_punk_m.png', $path2);

                break;
            case 'steamerF':
                //copy('../webroot/img/avatar/tete_punk_f.png', $path);
                copy('../webroot/img/avatar/fighter_punk_f.png', $path2);
                break;
            case 'papy':
                //copy('../webroot/img/avatar/tete_vieux_m.png', $path);
                copy('../webroot/img/avatar/fighter_vieux_m.png', $path2);
                break;
            case 'mamie':
                //copy('../webroot/img/avatar/tete_vieux_f.png', $path);
                copy('../webroot/img/avatar/fighter_vieux_f.png', $path2);
                break;
        }
        
        return $nomFighter;

    }
	
    
    
	public function fetchXY(){
        
        $xy = $this->find("first", array('fields' => array('Fighter.coordinate_x','Fighter.coordinate_y')));
        
        $coordinates = array('coordinate_x' => $xy['Fighter']['coordinate_x'], 'coordinate_y' => $xy['Fighter']['coordinate_y']);
        
        return $coordinates;
    }
    
    public function fiche_personnage($pid){
        
        
        
        $donnees = $this->find("first",
                ['fields' => ['Fighter.name', 'Fighter.level', 'Fighter.xp', 
            'Fighter.skill_sight','Fighter.skill_strength', 'Fighter.skill_health', 'Fighter.current_health'],
                'conditions'=>['Fighter.player_id'=>$pid]]);

        $nom = $donnees['Fighter']['name'];
        $lvl = $donnees['Fighter']['level'];
        $xp = $donnees['Fighter']['xp'];
        $skill_sight = $donnees['Fighter']['skill_sight'];
        $skill_strength = $donnees['Fighter']['skill_strength'];
        $skill_health = $donnees['Fighter']['skill_health'];
        $current_health = $donnees['Fighter']['current_health'];
        
        
        $fiche = array('nom' => $nom, 'level' => $lvl, 'xp' => $xp,
            'sight' => $skill_sight, 'strength' => $skill_strength,
            'skill_health' => $skill_health, 'health' => $current_health);
        
        return $fiche;
    }
}

