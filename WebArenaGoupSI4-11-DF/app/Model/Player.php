<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppModel', 'Model');
App::uses('Security', 'Utility');

class Player extends AppModel {

    public $displayField = 'name';

//Si le joueur possède un ou des personnages on lui renvoit la liste de ses persos
//Sinon on renvoit null pour qu'il puisse en créer un



    public function createNew($mail, $pwd)
    {
        if ($this->find('first',
                        array('conditions' => array('Player.email' => $mail), 'fields' => 'Player.email')) == NULL)
        {
            $hashpwd = Security::hash($pwd);
            $this->save(Array('email' => $mail,
                'password' => $hashpwd));
            return true;
        } else
        {
            return false;
        }
    }

    public function checkLogin($login, $pwd)
    {
        $hashpwd = Security::hash($pwd);

        //On récupère juste l'email et on laisse apache tester le mot de passe;
        $data = $this->find('first',
                array('conditions' => array('Player.email' => $login)));
        if (count($data) != 0)
        {
            if ($data['Player']['password'] != $hashpwd)
            {
                return false;
            } else
            {
                return $data['Player']['id'];
            }
        }
        else{
            return false;
        }
    }

    public function getPassword($login)
    {

        $data = $this->find('first',
                array('conditions' => array('Player.email' => $login)));
        //Si ma requête a réussi
        if (isset($data))
        {
            //Je renvoie le password doublement crypter
            $data['Player']['password'] = Security::hash($data['Player']['password']);
            return $data;
        }
        //Sinon je renvoie faux
        else
        {

            return false;
        }
    }

}
