<?php

App::uses('AppModel', 'Model');

class Surrounding extends AppModel {
    
    
    public function set_surroundings(){
        
        $this->deleteAll('1 = 1');
 
        for($y = 0; $y < 10; $y++){
            for($x = 0; $x < 15; $x++){
                
                $this->create();
                $this->save($this->data);
                
                $random = rand(0,3);
                
                if($x==5 && $y==8){
                    $random=6;
                    $this->set('type','monstre');
                    $this->set('coordinate_x',$x);
                    $this->set('coordinate_y',$y);
                    $this->save($this->data);
                }
                
                //Les colonnes
                if ($random==0){
                    $this->set('type','colonne');
                    $this->set('coordinate_x',$x);
                    $this->set('coordinate_y',$y);
                    for($a=1;$a<11;$a++){
                        $s=$x-$a;
                        $d=$y-$a;
                        if($s>=0){
                            if(count
                                    ($this->find("first",
    ['fields' => (['Surrounding.type']),
        'conditions'=>  ["Surrounding.type"=>'colonne',"Surrounding.coordinate_x"=>$s,"Surrounding.coordinate_y"=>$y]]))!=0){
                                $this->set('type','grass');
                            }
                        }
                        if($d>=0){
                            if(count
                                    ($this->find("first",
    ['fields' => (['Surrounding.type']),
        'conditions'=>  ["Surrounding.type"=>'colonne',"Surrounding.coordinate_x"=>$x,"Surrounding.coordinate_y"=>$d]]))!=0){
                                $this->set('type','grass');
                            }
                        }
                    }
                    $this->save($this->data);
                }
                
                //Les pièges
                elseif($random==1){
                    $this->set('type','piege');
                    $this->set('coordinate_x',$x);
                    $this->set('coordinate_y',$y);
                    for($a=1;$a<11;$a++){
                        $s=$x-$a;
                        $d=$y-$a;
                        if(($s)>=0){
                             if(count
                                    ($this->find("first",
    ['fields' => (['Surrounding.type']),
        'conditions'=>  ["Surrounding.type"=>'piege',"Surrounding.coordinate_x"=>$s,"Surrounding.coordinate_y"=>$y]]))!=0){
                                $this->set('type','grass');
                            }
                        }
                        if(($d)>=0){
                            if(count
                                    ($this->find("first",
    ['fields' => (['Surrounding.type']),
        'conditions'=>  ["Surrounding.type"=>'piege',"Surrounding.coordinate_x"=>$x,"Surrounding.coordinate_y"=>$d]]))!=0){
                                $this->set('type','grass');
                            }
                        }
                        }
                        $this->save($this->data);
                    }
                    
                    elseif($random>1 && $random<6  ){
                    $this->set('type','grass');
                    $this->set('coordinate_x',$x);
                    $this->set('coordinate_y',$y);
                    $this->save($this->data);
                    }
                    
                }
  
        }
        
        
       
    }
    
    
}
