<?php

App::uses('AppModel', 'Model');

class Event extends AppModel {
    
    
    public function newEvent($description, $x, $y){
                
                $datetime = date("Y-m-d H:i:s");
                
                
                $this->create(); 
                $this->save($this->data);
                    
                    $this->set('name', $description);
                    $this->set('date', $datetime);
                    $this->set('coordinate_x', $x);
                    $this->set('coordinate_y', $y);
                   
            
                $this->save($this->data);
                return true;
         }
    
    
    
}