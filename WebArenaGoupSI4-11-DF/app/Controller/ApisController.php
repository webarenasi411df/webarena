<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppController', 'Controller');

class ApisController extends AppController {
 
    public $uses = array('Player', 'Fighter', 'Event');
    public function index() {
                        
    }
    public function fighterView($id) {
        $this->layout = 'ajax'; 
        $this->set('datas', $this->Fighter->find('first',array('conditions'=>array('Fighter.id'=>$id))));
    }
    public function fighterDoMove($id,$direction) {
        $this->layout = 'ajax'; 
        $this->set('datas', $this->Fighter->find('first',array('conditions'=>array('Fighter.id'=>$id))));
        
    }
    public function fighterDoAttack($id,$direction) {
        $this->layout = 'ajax'; 
        $this->set('datas', $this->Fighter->find('first',array('conditions'=>array('Fighter.id'=>$id))));
    }

}