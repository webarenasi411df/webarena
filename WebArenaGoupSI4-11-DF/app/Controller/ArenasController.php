<?php

App::uses('AppController', 'Controller');

/**
 * Main controller of our small application
 * Mot ed passe ot1pGDD17Z
 * @author ...
 */
class ArenasController extends AppController {

    /**
     * index method : first page
     *
     * @return void
     */
    public $uses = array('Player', 'Fighter', 'Event', 'MailManagement', 'Surrounding');
    public $components = array('Password', 'Log');

    public function index()
    {
        $this->set('myname', 'Team 11');
        $this->Log->checklogin($this);
    }
    public function beforeFilter()
    {
        parent::beforeFilter();

        //Si la fonction est différente de login 
        if (!$this->Session->check('Connected') AND $this->request->params['action'] != 'login')
        {
            //Accéder au page avant la connexion
            if ($this->request->params['action'] == 'index' OR $this->request->params['action'] == 'subscribe'
                    OR $this->request->params['action'] == 'login')
            {
                
            }
            //Sinon on est redirigé vers la page de login pour out autre action
            else
            {
                $this->redirect(array('controller' => 'Arenas', 'action' => 'login'));
            }
        }
        //SI on est connecté on peut accéder au page de l'arène
        else if ($this->Session->check('Connected') AND ( $this->request->params['action'] == 'login' OR
                $this->request->params['action'] == 'index' OR $this->request->params['action'] == 'subscribe'))
        {
          
//                if (count($this->Fighter->havePerso($this->Session->read('Connected'))) == 0)
//            {
//                $this->redirect(array('controller' => 'Arenas', 'action' => 'createFighter'));
//            } else
//            {
//                $this->redirect(array('controller' => 'Arenas', 'action' => 'fighter'));
//            }
            
            
        }
    }

    public function createFighter()
    {

        if ($this->request->is('post'))
        {


            if (isset($this->request->data['createFighter']))
            {

                $this->Session->setFlash('Vous venez de créer un combattant de la classe: ' . $this->request->data['createFighter']['myimage'],
                        'flash/flashok');

                        $this->Fighter->newFighter($this->request->data['createFighter']['name'],
                        $this->request->data['createFighter']['myimage'],
                        $this->Session->read('Connected'));
                
                

                $this->redirect(array('controller' => 'Arenas', 'action' => 'fighter'));
            }

            $this->Log->checklogout($this);
        }
    }

    public function fighter()
    {
        $pid = $this->Session->read('Connected');
        $donnees = $this->Fighter->find("first",
                ['fields' => ['Fighter.id'],
                'conditions'=>['Fighter.player_id'=>$pid]]);
        $id = $donnees['Fighter']['id'];
        
        if ($this->request->is('post'))
        {

            if (isset($this->request->data['levelupFighter']))
            {
                $this->Session->setFlash('Up of your character.',
                        'flash/flashinfos');
                $this->Fighter->levelUp($id,
                        $this->request->data['levelupFighter']['choice']);
            }

            if (isset($this->request->data['createFighter']))
            {
                $this->Session->setFlash('Vous venez de créer un combattant',
                        'flash/flashok');
                $this->Fighter->newFighter($this->request->data['createFighter']['name'],
                        $this->request->data['createFighter']['avatar']);
            }
            $this->Log->checklogout($this);
        }
        //die('test');
        $this->set('raw', $this->Fighter->find('all'));
        
        $pid = $this->Session->read('Connected');
        $fiche = $this->Fighter->fiche_personnage($pid);
        $this->set('fiche',$fiche);
        $this->set('id', $id);
        
    }

    public function index_events()
    {
        $this->set('events', $this->Event->find('all'));

        if (isset($this->params['requested']))
        { //s’il s’agit de l’appel pour l’élément
            $rubriques_events = $this->Event->find('all', array('limit' => 20, 'order' => 'Event.date DESC'));
            return $rubriques_events;
        }
    }

    public function diary()
    {
        $this->set('raw', $this->Event->find());

        //Gestion du journal d'évènements
        if (isset($this->request->data['Fightermove']))
        { //Si fighter se déplace
            $description = 'The fighter moved to the ' . $this->Session->read('Fightermove');

            $x = $this->Session->read('Fighter_x');
            $y = $this->Session->read('Fighter_y');

            $this->Event->newEvent($description, $x, $y);
        } elseif (isset($this->request->data['Fighterattack']))
        { //Si fighter attaque
            $outcome = $this->Session->read('Fighterattack_outcome');

            if ($outcome == 1)
            { //Si fighter gagne l'attque
                $description = 'Your fighter launched an attack to the ' . $this->Session->read('Fighterattack_dir') . ' You defeated ' . $this->Session->read('Fighterattack_name');
            } else
            { //Si fighter perd
                $description = 'Your fighter launched an attack to the ' . $this->Session->read('Fighterattack_dir') . '. You were defeated by ' . $this->Session->read('Fighterattack_name');
            }

            $x = $this->Session->read('Fighter_x');
            $y = $this->Session->read('Fighter_y');

            $this->Event->newEvent($description, $x, $y);
        } elseif (isset($this->request->data['levelupFighter']))
        { //Si fighter gagne un niveau
            $description = 'Congratulations, your fighter passed a new level! Bonus: ' . $this->Session->read('Fighterlevelup');

            $x = $this->Session->read('Fighter_x');
            $y = $this->Session->read('Fighter_y');

            $this->Event->newEvent($description, $x, $y);
        } elseif (isset($this->request->data['createFighter']))
        { //Et si un nouveau fighter est créé
            $description = 'You created a new fighter called ' . $this->Session->read('Fightercreation');

            $x = $this->Session->read('Fighter_x');
            $y = $this->Session->read('Fighter_y');

            $this->Event->newEvent($description, $x, $y);
        }
        $this->Log->checklogout($this);
        $this->set('raw', $this->Event->find('all'));
    }

    public function login()
    {
        if ($this->request->is('post'))
        {
            if (!empty($this->request->data['Login']))
            {
                $id = $this->Player->checkLogin($this->request->data['Login']['email'],
                        $this->request->data['Login']['password']);
                if (!$id)
                {
                    $this->Session->setFlash('Mauvais mot de passe ou email',
                            'flash/flashdanger');
                } else
                {

                    $this->Session->write('Connected', $id);
                    $this->redirect(array('controller' => 'Arenas', 'action' => 'fighter'));
                }
            }
            if (!empty($this->request->data['Facebook']))
            {
               
                $id = $this->Player->checkLogin($this->request->data['Facebook']['email'],
                        $this->request->data['Facebook']['password']);
                
         
                if (!$id)
                {

                    if ($this->Player->createNew($this->request->data['Facebook']['email'],
                                    $this->request->data['Facebook']['password']))
                    {
                        $this->Session->setFlash('Votre inscription a réussi',
                                'flash/flashok');
                         $this->Session->write('Connected', $id);
                        //On redirige sur la page de création du Fighter                     
                            $this->redirect(array('controller' => 'Arenas', 'action' => 'createFighter'));
                       
                    } else
                    {
                        $this->Session->setFlash('Inscription: échec',
                                'flash/flashdanger');
                    }
                } else
                {


                    $this->Session->write('Connected', $id);
                 
                   //On le dirige sur la fiche de son personnage
                        $this->redirect(array('controller' => 'Arenas', 'action' => 'fighter'));
                    
                }
            }
            $this->Log->checklogin($this);
        }
    }

    public function logOut()
    {
      
        if ($this->request->is('post'))
        {

            if (!empty($this->request->data['logOut']))
            {
                $this->Session->delete('Connected');
                $this->redirect(array('controller' => 'Arenas', 'action' => 'index'));
            }
            $this->Log->checklogout($this);
        }
    }

    public function subscribe()
    {
        if ($this->request->is('post'))
        {

            if (!empty($this->request->data['MailManagement']))
            {
                $password = $this->Password->generatePassword();
                //Création du mail
                //Si le mail est valide on crée un nouveau compte +envoie de mail sinon e-mail invalide


                if ($this->Player->createNew($this->request->data['MailManagement']['email'],
                                $password))
                {
                    if (!$this->MailManagement->sendSubcribingEmail($this->request->data['MailManagement']['email'],
                                    $password))
                    {
                        $this->Session->setFlash('email invalide',
                                'flash/flashdanger');
                    } else
                    {
                        $this->Session->setFlash('Un email vient de vous être envoyer',
                                'flash/flashok');
                    }
                } else
                {
                    $this->Session->setFlash('Cet email existe déjà',
                            'flash/flashdanger');
                }
            }
            $this->playerid = $this->Log->checklogin($this);
        }
    }

    public function recover($email, $pwd)
    {
        if ($this->request->is('post'))
        {
            if (!empty($this->request->data['MailManagement']))
            {
                
            }
        }
    }


    public function sight() {
        

        
        //**********Gestion des formulaires de mouvement et d'attaque*********************
        
        
        
        if ($this->request->is('post')) {
            
            $pid = $this->Session->read('Connected');
            $coord_fighter = $this->Fighter->find("first",
                ['fields' => ['Fighter.id','Fighter.name', 'Fighter.skill_sight','Fighter.coordinate_x', 'Fighter.coordinate_y'],
                'conditions'=>['Fighter.player_id'=>$pid]]);
            $id = $coord_fighter['Fighter']['id'];
            //$coordinates = $this->Fighter->fetchXY();
            $x = $coord_fighter['Fighter']['coordinate_x'];
            $y = $coord_fighter['Fighter']['coordinate_y'];
            
            if (isset($this->request->data['Fightermove'])) {
                

                $this->Fighter->doMove($id, $this->request->data['Fightermove']['direction']);
                

                $this->Session->setFlash('Your fighter has moved to the ' . $this->request->data['Fightermove']['direction'],
                        'flash/flashok');

                $this->Session->write('Fightermove', $this->request->data['Fightermove']['direction']);
                $this->Session->write('Fighter_x', $x);
                $this->Session->write('Fighter_y', $y);

                $this->diary();

            } elseif (isset($this->request->data['Fighterattack'])) {

                $result = $this->Fighter->doAttack($id, $this->request->data['Fighterattack']['direction']);

                $this->Session->write('Fighterattack_dir', $this->request->data['Fighterattack']['direction']);
                $this->Session->write('Fighterattack_name', $result['name_opponent']);
                $this->Session->write('Fighterattack_outcome', $result['attack_outcome']);

                if ($result['attack_outcome'] == 1) {


                    $this->Session->setFlash('Your fighter launched an attack to the ' . $this->request->data['Fighterattack']['direction'] . '. You defeated ' . $result['name_opponent'],
                            'flash/flashok');

                    //$this->Session->write('Fighterattack_outc', $result['attack_outcome']);
                    $this->Session->write('Fighter_x', $x);
                    $this->Session->write('Fighter_y', $y);

                    $this->diary();
                    
                } elseif ($result['attack_outcome'] == 0) {

                    $this->Session->setFlash('Your fighter launched an attack to the ' . $this->request->data['Fighterattack']['direction'] . '. You were defeated by ' . $result['name_opponent']
                            , 'flash/flashdanger');

                    //$this->Session->write('Fighterattack_outc', $result['attack_outcome']);
                    $this->Session->write('Fighter_x', $x);
                    $this->Session->write('Fighter_y', $y);
                    
                    /*$death = $this->Fighter->deathFighter($id);
                    if($death == 1){
                        
                        $this->redirect(array('controller' => 'Arenas', 'action' => 'fighter'));
                    }*/

                    $this->diary();
                }
                
            }elseif (isset($this->request->data['Newarena'])) {
                $this->Surrounding->set_surroundings();
                
            }
                     
        }
        
        //******************************Gestion de l'arène**********************************************************************
        $this->set('raw', $this->Fighter->find('all'));
        $pid = $this->Session->read('Connected');
        $coord_fighter = $this->Fighter->find("first",
                ['fields' => ['Fighter.id','Fighter.name', 'Fighter.skill_sight','Fighter.coordinate_x', 'Fighter.coordinate_y'],
                'conditions'=>['Fighter.player_id'=>$pid]]);
        $id = $coord_fighter['Fighter']['id'];

        
        $tab = array();
        
        $coord_elmt = $this->Surrounding->find('all', array('fields' => array('Surrounding.type','Surrounding.coordinate_x', 'Surrounding.coordinate_y')));

        $coord_colonne = $this->Surrounding->find('all', array('fields' => array('Surrounding.coordinate_x', 'Surrounding.coordinate_y'),
            'conditions' => array('Surrounding.type' => 'colonne')));
        
        $coord_piege = $this->Surrounding->find('all', array('fields' => array('Surrounding.coordinate_x', 'Surrounding.coordinate_y'),
            'conditions' => array('Surrounding.type' => 'piege')));
        
        $coord_puanteur = $this->Surrounding->find('all', array('fields' => array('Surrounding.coordinate_x', 'Surrounding.coordinate_y'),
            'conditions' => array('Surrounding.type' => 'puanteur')));
        
        $coord_monstre = $this->Surrounding->find('first', array('fields' => array('Surrounding.coordinate_x', 'Surrounding.coordinate_y'),
            'conditions' => array('Surrounding.type' => 'monstre')));

        
        $fighter_x = $coord_fighter['Fighter']['coordinate_x'];
        $fighter_y = $coord_fighter['Fighter']['coordinate_y'];
        $fighter_sight = $coord_fighter['Fighter']['skill_sight'];
        $fighter_name = $coord_fighter['Fighter']['name'];
        //$fighter_id = $coord_fighter['Fighter']['id'];
        
        
         $coord_opponent = $this->Fighter->find('all', array('fields' => array('Fighter.coordinate_x', 'Fighter.coordinate_y'),
            'conditions' => array('Fighter.id !=' => $id)));
        
        $colonne_x = 0;
        $colonne_y =0;
        
        $puanteur_x = 0;
        $puanteur_y =0;
        
        $piege_x = 0;
        $piege_y = 0;
        
        $direction = $this->Session->read('Fightermove');
        
        for ($j = 0; $j < 10; $j++) {

            for ($i = 0; $i < 15; $i++) {

                for ($a = 0; $a < sizeof($coord_elmt); $a++) {

                    $elmt_x = $coord_elmt[$a]['Surrounding']['coordinate_x'];
                    $elmt_y = $coord_elmt[$a]['Surrounding']['coordinate_y'];
                    $elmt_type = $coord_elmt[$a]['Surrounding']['type'];
                    
                    $tab_elmt[$elmt_x][$elmt_y] = $elmt_type;
                    
                    $tab[$i][$j] = 'gris';
                    
                    for($utilx=0;$utilx<=$fighter_sight;$utilx++){
                        for($utily=0;$utily<=$fighter_sight;$utily++){
                            
                            if($j==($fighter_y+$utily) && $i==($fighter_x+$utilx) ){
                            $tab[$i][$j] = 'grass';
                        }
                        elseif($j==($fighter_y+$utily) && $i==($fighter_x-$utilx) ){
                            $tab[$i][$j] = 'grass';
                        }
                        elseif($j==($fighter_y-$utily) && $i==($fighter_x+$utilx) ){
                            $tab[$i][$j] = 'grass';
                        }
                        elseif($j==($fighter_y-$utily) && $i==($fighter_x-$utilx) ){
                            $tab[$i][$j] = 'grass';
                        }
                        
                         
                        }
                        
                    }

    if($tab[$i][$j] != 'gris'){
        
                    if (($i == $coord_fighter['Fighter']['coordinate_x']) && ($j == $coord_fighter['Fighter']['coordinate_y'])) {

                                $tab[$i][$j] = 'fighter';
                    } else {
                            
                        for ($b = 0; $b < sizeof($coord_opponent); $b++) {

                                $opponent_x = $coord_opponent[$b]['Fighter']['coordinate_x'];
                                $opponent_y = $coord_opponent[$b]['Fighter']['coordinate_y'];
                        

                                if (($i == $opponent_x ) AND ($j == $opponent_y)) {

                                    $tab[$i][$j] = 'opponent';
                                    //pr($tab[$i][$j].$i.' '.$j);
                                }
                        }
                    
                        for ($x = 0; $x < sizeof($coord_colonne); $x++) {

                                $colonne_x = $coord_colonne[$x]['Surrounding']['coordinate_x'];
                                $colonne_y = $coord_colonne[$x]['Surrounding']['coordinate_y'];
                        


                                if (($i == $colonne_x ) AND ($j == $colonne_y)) {

                                    $tab[$i][$j] = 'colonne';
                                    //pr($tab[$i][$j].$i.' '.$j);
                                }

                                
                                
                                
                                
                        }
                    
                        for ($y = 0; $y < sizeof($coord_piege); $y++) {

                            $piege_x = $coord_piege[$y]['Surrounding']['coordinate_x'];
                            $piege_y = $coord_piege[$y]['Surrounding']['coordinate_y'];
                        

                            if (($i == $piege_x ) AND ($j == $piege_y)) {

                                $tab[$i][$j] = 'piege';
                                //pr($tab[$i][$j].$i.' '.$j);
                            }
                            
                            if(($fighter_x == $piege_x ) AND ($fighter_y == $piege_y)){
                            
                            $this->Fighter->deathFighterArena($id);
                            $this->redirect(array('controller' => 'Arenas', 'action' => 'createFighter'));
                            
                            }
                        }
                    
                        for ($k = 0; $k < sizeof($coord_puanteur); $k++) {

                            $puanteur_x = $coord_puanteur[$k]['Surrounding']['coordinate_x'];
                            $puanteur_y = $coord_puanteur[$k]['Surrounding']['coordinate_y'];
                        

                            if (($i == $puanteur_x ) AND ($j == $puanteur_y)) {

                                $tab[$i][$j] = 'puanteur';
                                //pr($tab[$i][$j].$i.' '.$j);
                            }
                        
                            if(($fighter_x == $puanteur_x ) AND ($fighter_y == $puanteur_y)){
                            
                            $this->Session->setFlash('Puanteur!');
                            
                            }
                        }
                    
                    $monstre_x = $coord_monstre['Surrounding']['coordinate_x'];
                    $monstre_y = $coord_monstre['Surrounding']['coordinate_y'];
                        
                    if(($i == $monstre_x ) AND ($j == $monstre_y)){
                            
                            $tab[$i][$j] = 'monstre';
                            
                            
                    }
                    
                    if(($fighter_x == $monstre_x ) AND ($fighter_y == $monstre_y)){
                            
                            $this->Fighter->deathFighterArena($id);
                            $this->redirect(array('controller' => 'Arenas', 'action' => 'createFighter'));
                            
                    }
                }
                
               
              }
            }
        }
        }
        
        if( ($tab_elmt[$fighter_x][$fighter_y]=='colonne') ){
                                        
            if($direction == 'north'){
                $this->Fighter->doMove($id, 'south');
            }elseif($direction == 'south'){
                $this->Fighter->doMove($id, 'north');
            }elseif($direction == 'west'){
                $this->Fighter->doMove($id, 'east');
            }elseif($direction == 'east'){
                $this->Fighter->doMove($id, 'west');
            }
                                            
            $this->redirect(array('controller' => 'Arenas', 'action' => 'sight'));
                                    
        }elseif( ($tab_elmt[$fighter_x+1][$fighter_y]=='piege') || 
                    ($tab_elmt[$fighter_x][$fighter_y+1]=='piege') ||
                        ($tab_elmt[$fighter_x-1][$fighter_y]=='piege') ||
                            ($tab_elmt[$fighter_x][$fighter_y-1]=='piege') ){
            
            $this->Session->setFlash('Brise suspecte!');
            
        }elseif( ($tab_elmt[$fighter_x+1][$fighter_y]=='monstre') || 
                    ($tab_elmt[$fighter_x][$fighter_y+1]=='monstre') ||
                        ($tab_elmt[$fighter_x-1][$fighter_y]=='monstre') ||
                            ($tab_elmt[$fighter_x][$fighter_y-1]=='monstre') ){
            
            $this->Session->setFlash('Puanteur!');
            
        }
        
        //pr($tab_elmt);
        $this->set('tab', $tab);
        $this->set('fighter_name', $fighter_name);
        $this->set('id', $id);
        $this->Log->checklogout($this);
        
    }

}

?>