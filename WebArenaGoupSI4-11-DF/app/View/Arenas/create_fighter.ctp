<?php
$this->layout = 'arena';
//Si le joueur n'a pas de personnage
//On affiche la page de création de personnage



$this->assign('title', 'createFighter');

?>
<div class="row">
    <h1 class="col-lg-offset-3 col-lg-6">
    Créer votre personnage
</h1>
</div>
<div class="row">
    
</div>
<?php
echo $this->Form->create('createFighter',array('class'=>array('col-lg-offset-4','col-lg-3',
    'col-md-offset-4','col-md-4','col-sm-offset-4','col-sm-4 ')));

echo $this->Form->input('name',array('label'=>'Nom','class'=>'form-control','div'=>'form-group',
       'placeholder'=>'Barbe Noire','required' ));
echo $this->Form->hidden('myimage');
?>
<div class="form-group">
    <div id="myCarousel" class="carousel slide" data-interval="false" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
            <li data-target="#myCarousel" data-slide-to="3"></li>
            <li data-target="#myCarousel" data-slide-to="4"></li>
            <li data-target="#myCarousel" data-slide-to="5"></li>

        </ol>
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <?php echo $this->Html->image('./avatar/sacrieurM.png',
                        array('alt' => 'sacrieurM', 'class' => 'img-responsive'));
                ?>
            </div>
            <div class="item">
                <?php echo $this->Html->image('./avatar/sacrieurF.png',
                        array('alt' => 'sacrieurF', 'class' => 'img-responsive'));
                ?>
            </div>
            <div class="item">
<?php echo $this->Html->image('./avatar/papy.png',
        array('alt' => 'papy', 'class' => 'img-responsive'));
?>
            </div>
            <div class="item">
                <?php echo $this->Html->image('./avatar/mamie.png',
                        array('alt' => 'mamie', 'class' => 'img-responsive'));
                ?>
            </div>
            <div class="item">
                <?php echo $this->Html->image('./avatar/steamerM.png',
                        array('alt' => 'steamerM', 'class' => 'img-responsive'));
                ?>
            </div>
            <div class="item">
<?php echo $this->Html->image('./avatar/steamerF.png',
        array('alt' => 'steamerF', 'class' => 'img-responsive'));
?>
            </div>

        </div>
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

</div>
<?php
$options=array('label'=>'Créer','class'=>array('btn','btn-primary'),'id'=>'Submit');
echo $this->Form->end($options);?>