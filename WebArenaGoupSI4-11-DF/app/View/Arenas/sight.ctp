<?php 
//pr($raw);
//<?php pr($raw); ?>

<?php $this->assign('title', 'sight');
//pr($fighter_name);
?>

<?php
$this->layout='arena';
$grass = $this->Html->image('./decors/grass.png');
$colonne = $this->Html->image('./decors/colonne.png');
$fighter = $this->Html->image('./avatar/' .$fighter_name. '.png');
$gris = $this->Html->image('./decors/bush.png');
$monstre = $this->Html->image('./decors/monstre.png');
$opponent = $this->Html->image('./avatar/opponent.png');

$piege = $this->Html->image('./decors/piege.png');    
?>


 <section class="col-lg-12 ">
  <table id = "arena">
<?php

for($y = 0; $y < 10; $y++){
    
    echo '<tr>';
    
    for($x = 0; $x < 15; $x++){
        
        echo '<td>';
        if( $tab[$x][$y] == 'fighter'){
            $arena[$x][$y] = $fighter;
        }elseif($tab[$x][$y] == 'colonne' ){
            $arena[$x][$y] = $colonne;
        }elseif($tab[$x][$y] == 'opponent' ){
            $arena[$x][$y] = $opponent;
        }elseif($tab[$x][$y] == 'monstre' ){
            $arena[$x][$y] = $monstre;
        }elseif( $tab[$x][$y] == 'piege' ){
            $arena[$x][$y] = $piege;
        }elseif($tab[$x][$y] == 'gris' ){
            $arena[$x][$y] = $gris;
        }elseif( $tab[$x][$y] == 'grass' || $tab[$x][$y] == 'puanteur'){
            $arena[$x][$y] = $grass;
        }
        echo $arena[$x][$y];
        echo '</td>'; 
             
    }
    echo '</tr>';  
}
?>
</table> 
 </section>

<?php
echo $this->Form->create('Fightermove', array('class' => "col-md-4"));
echo '<legend>Déplacement</legend>';
echo '<div class="form-group">';
echo $this->Form->input('direction',array('options' => array('north'=>'north','east'=>'east','south'=>'south','west'=>'west'), 
    'default' => 'east',
    'class' => "form-control"));
echo '</div>';
echo '<div class="form-group">';

echo $this->Form->input('fighterId',array('options' => array('1'=>$id),
    'default' => $id,
    'class' => "form-control"));
echo '</div>';
echo $this->Form->end('Move');
?>



<?php
echo $this->Form->create('Fighterattack', array('class' => "col-md-4"));
echo '<legend>Attaque</legend>';
echo '<div class="form-group">';
echo $this->Form->input('direction',array('options' => array('north'=>'north','east'=>'east','south'=>'south','west'=>'west'), 
    'default' => 'east',
    'class' => "form-control"));
echo '</div>';
echo '<div class="form-group">';

echo $this->Form->input('fighterId',array('options' => array('1'=> $id),
    'default' => $id,
    'class' => "form-control"));
echo '</div>';
echo $this->Form->end('Attack');
?>

<?php
echo $this->Form->create('Newarena', array('class' => "col-lg-4"));
echo '<legend>Générer une nouvelle arène</légende>';
echo '<div class="form-group">';
echo $this->Form->input('Nouvelle arene', array('type' => 'checkbox'));
echo '</div>';
echo $this->Form->end('Go');
?>
 
