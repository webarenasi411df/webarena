<?php
$this->assign('title', 'logOut');
$this->layout='arena';
?>


<?php echo $this->Form->create("logOut");?>

 <?php
                //J'ai rajouté une classe hidden pour transmettre une donnée nul lors du submit
                //Sinon aucune donnée n'est créer donc on ne peut pas savoir si le formulaire a bien
                //été cliqué face à d'autre formulaires.
                echo $this->Form->hidden('needit');?>
 
        <?php echo $this->Form->end("Déconnexion");?>