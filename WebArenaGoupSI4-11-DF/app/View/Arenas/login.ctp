<?php $this->assign('title', 'login'); ?>
<?php $this->layout = 'connexion' ?>



<div class="row">
    <?php
    echo $this->Form->create("Login",
            array('class' => array('col-lg-offset-3', 'col-lg-6')));
    ?>


    <?php
    echo $this->Form->input("email",
            array('label' => 'Votre email:', 'class' => 'form-control', 'div' => 'form-group',
        'placeholder' => 'exemple@exemple.com'))
    ?>
    <div class="form-group ">
        <?php echo $this->Form->label('Login.password',
                'Votre mot de passe:');
        ?>
        <?php
        echo $this->Form->password("password",
                array('label' => 'Votre login', 'class' => 'form-control', 'placeholder' => 'mot de passe'));
        ?>
    </div>


    <?php
    $options = array('label' => 'connexion', 'class' => array('btn', 'btn-primary'), 'div' => false);
    echo $this->Form->end($options);
    ?>
</div>

<div class="row">
    <div class="col-lg-offset-4">
        <a href="#" id='cofacebook' class="btn btn-group btn-social btn-facebook ">
            <i class="fa fa-facebook"></i> Login with Facebook
        </a>

<!--        <span id="signinButton" class="btn-group">
            <span
                class="g-signin" 
                onclick="signinCallback"
                data-clientid="697732429265-omis92fm6rtkceielt67eme4nj2rv8n7.apps.googleusercontent.com"
                data-cookiepolicy="single_host_origin"
                data-requestvisibleactions="http://schemas.google.com/AddActivity"
                data-scope="https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email">
            </span>
        </span>     -->
    </div>
</div>





</div>







<?php
echo $this->Form->create("Facebook",
        array('class' => array('col-lg-offset-3', 'col-lg-6')));
?>
<!--On injectera du code html dans ces cellules aider de Louis-AG-->
<div id="emailface"></div>
<div id="pwdface"></div>
<?php
$options = array('label' => 'connexion', 'style' => 'display: none', 'id' => 'Fbco');
echo $this->Form->end($options);
?><div id="status">
</div>