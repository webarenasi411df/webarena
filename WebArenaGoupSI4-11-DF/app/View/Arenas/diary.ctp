<?php $this->assign('title', 'Diary');?>
<?php
$this->layout='arena';
$rubriques = $this->requestAction ('arenas/index_events');?>

<table class="table table-bordered table-striped table-responsive">
    <tr>
        <th>Action</th>
        <th>Date</th>
        <th>Position x</th>
        <th>Position y</th>
    </tr>

    <?php if ($rubriques) { 
        foreach ($rubriques as $rubrique) {
    ?>
    <tr>
        <td><?php echo $rubrique['Event']['name']; ?></td>
        <td><?php echo $rubrique['Event']['date']; ?></td>
        <td><?php echo $rubrique['Event']['coordinate_x']; ?></td>
        <td><?php echo $rubrique['Event']['coordinate_y']; ?></td>
    </tr>
    <?php }
    } 
?>
</table>