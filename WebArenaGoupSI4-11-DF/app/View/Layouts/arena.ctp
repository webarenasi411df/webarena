<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>



<!DOCTYPE html>
<html lang="fr">
  <head>
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
   
     <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo "WebArenaGroupSI4-11-DF" ?>:
            <?php echo $this->fetch('title'); ?>
        </title>
        <?php
        echo $this->Html->meta('icon');

        echo $this->Html->css('bootstrap.min');
        echo $this->Html->css('bootstrap');
        echo $this->Html->css('bootstrap-theme.min');

        echo $this->fetch('meta');
        echo $this->fetch('css');
        ?>
    <title><?php echo $title_for_layout;?></title>

    <!-- Bootstrap core CSS -->
    <link href="../../dist/css/bootstrap.min.css" rel="stylesheet">



    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
      
    <div class="container">
    <div class="row">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
                            <?php echo $this->html-> link ('WebArena', array ('controller' => 'Arenas', 'action' => '/'), array('class'=>'navbar-brand')) ; ?>

        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
       
            <li><?php echo $this->html-> link ('Personnage', array ('controller' => 'Arenas', 'action' => 'fighter')) ; ?></li>
            <li><?php echo $this->html-> link ('Arène', array ('controller' => 'Arenas', 'action' => 'sight')) ; ?></li>
           
            <li><?php echo $this->html-> link ('Journal', array ('controller' => 'Arenas', 'action' => 'diary')) ; ?></li>
        
            
            <li><?php echo $this->html-> link ('Déconnexion', array ('controller' => 'Arenas', 'action' => 'logOut')) ; ?></li>
           
          </ul>
            <?php
                    echo $this->Form->create('LogOutHeader',
                            array('class' => array('navbar-form', 'navbar-right', 'inline-form')));
                    
                    ?>
            <div class="form-group">
                <?php
                //J'ai rajouté une classe hidden pour transmettre une donnée nul lors du submit
                //Sinon aucune donnée n'est créer donc on ne peut pas savoir si le formulaire a bien
                //été cliqué face à d'autre formulaires.
                echo $this->Form->hidden('needit');?>
                 <?php $options = array('label' =>  'Déconnexion','name'=>'logoutheader',
                            'div' => false, 'class' => array('btn', 'btn-primary', 'btn-sm'));
                        echo $this->Form->end($options);?>
                            </div>
        </div><!--/.nav-collapse -->
    </nav>

    
    <section class="col-lg-12">
        
        
          <div class="row">
            
            <article class="col-lg-12">
              <?php echo $this->Session->flash();?>
              <?php echo $this->fetch('content');?>
            </article>
            
          </div>

        
    </section>
    </div>
    <!-- /.container -->
    

  
    <footer class="row col-md-12">
     <?php
            //@todo ne pas oublier de mettre le lien de suivi de version et l'adresse du site
            echo 'WebArenaGroupSI4-11-DF';
            ?>
    <?php echo ' Mathieu Coche Nancy Oliele Emma-Louise Scappaticci Garreau Emilien '; ?>
    <ol>
        <li>  Gestion d'éléments du décors</li> 
        <li> Utilisation de Bootstrap </li> 
        <li>Utilisation d'une connexion externe Google/Facebook</li> 
    </ol>
        <p> <?php echo 'tentative d hebergement: http://webarena.esy.es/ '?> </p>
        <p>   <?php echo 'Voir log git dans version.log.txt url:https://bitbucket.org/webarenasi411df'?> </p>
    <?php echo $this->Html->script('http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js');?>
    <?php echo $this->Html->script('webarena'); ?>
        

<?php echo $this->Html->script('bootstrap.min'); ?>

<?php echo $this->fetch('script'); ?>
    </footer>     
  </body>
</html>
